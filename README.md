# Swisscom Remote Session Repository

Welcome to the repository for the Swisscom remote sessions. This repository contains the presentations and hands-on materials used during the sessions.

## Contents

- **Presentations:** Slides and other presentation materials used during the session.
- **Hands-On Materials:** Exercises and related files for practical, hands-on learning.
