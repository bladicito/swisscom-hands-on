# Gitlab hands-on (2nd session 2024-06-06)

## Prerequisities:

### Install GIT

```bash
# Install GIT on RedHat/CentOS/Rocky
yum install git

# Install GIT on Ubuntu/Debian
apt install git
```

Add the following lines to your `~/.bashrc` file to set up your Git author and committer information:

```bash
export GIT_AUTHOR_NAME="Name Surname"
export GIT_AUTHOR_EMAIL=name@domain.tld
export GIT_COMMITTER_NAME="$GIT_AUTHOR_NAME"
export GIT_COMMITTER_EMAIL="$GIT_AUTHOR_EMAIL"
```

After updating `~/.bashrc`, reload it with:

```bash
source ~/.bashrc
```

### Generate SSH key pair

#### Modern (safer cryptographic method)

Generate an Ed25519 key pair:

```bash
ssh-keygen -t ed25519 -C "your-email@example.com"
```

#### Traditional Solution

Generate an RSA key pair:

```bash
ssh-keygen -t rsa -b 4096 -C "your_email@example.com"
```

### Configure Gitlab Account

1. Copy your SSH public key to the clipboard:

For Ed25519:
```bash
cat ~/.ssh/id_ed25519.pub
```

For RSA:

```bash
cat ~/.ssh/id_rsa.pub
```

2. Go to Gitlab, navigate to **User Settings** > **SSH Keys**, and paste the key there.

### WSL users

You might need to use keychain, see https://esc.sh/blog/ssh-agent-windows10-wsl2/

```bash
sudo apt-get install keychain
```

You can load the key to memory like this:

```bash
/usr/bin/keychain -q --nogui $HOME/.ssh/id_rsa
source $HOME/.keychain/$(hostname)-sh
```
